# Social network

[![pipeline status](https://gitlab.com/adamward459/social-network-server/badges/main/pipeline.svg)](https://gitlab.com/adamward459/social-network-server/-/commits/main)

[![coverage report](https://gitlab.com/adamward459/social-network-server/badges/main/coverage.svg)](https://gitlab.com/adamward459/social-network-server/-/commits/main)

[![Latest Release](https://gitlab.com/adamward459/social-network-server/-/badges/release.svg)](https://gitlab.com/adamward459/social-network-server/-/releases)

## Installation

Requirements:

- Docker
- Yarn

1. Install dependencies: `yarn install`
2. Open a new terminal and run:

```shell
docker compose -f docker-compose.local.yaml up
```

This will create a postgres server and a redis server.

## System Design

### Supported Features

- Login/register
- 1 on 1 chat

### How to run tests

Run tests (unit test and integration test)
```shell
yarn test
```

Run e2e tests
```shell
yarn test:e2e
```

### Storage

The choice goes to a nosql database especially Mongodb (familiar), because what we need:

- Support big data and random
- Super low latency
- Support horizontal scaling
- Adopted by Facebook or Discord (for chat system)

## Database Design

```mermaid
erDiagram
  users {
    id objectId PK
    first_name string
    middle_name string "nullable"
    last_name string
    email string "unique"
    password string
    is_verified boolean
    last_active_at timestamp
    deleted_at timestamp "nullable"
    updated_at timestamp "default: timestamp"
    created_at timestamp "default: timestamp"
  }

  conversations {
    id objectId PK
    name string
    creator_id objectId FK "users.id"
    deleted_at timestamp "nullable"
    updated_at timestamp "default: timestamp"
    created_at timestamp "default: timestamp"
  }

  participants {
    user_id objectId FK "PK"
    conversation_id objectId FK "PK"
    updated_at timestamp "default: timestamp"
    created_at timestamp "default: timestamp"
  }

  messages {
    id serial PK
    message string
    sender_id objectId FK
    conversation_id objectId FK
    updated_at timestamp "default: timestamp"
    created_at timestamp "default: timestamp"
  }

  secrets {
    id objectId PK
    value string
    type string
    user_id objectId FK "PK"
    updated_at timestamp "default: timestamp"
    created_at timestamp "default: timestamp"
  }

  refresh_tokens {
    id objectId PK
    value text
    user_id objectId FK
    device string
    deleted_at timestamp
    created_at timestamp "default: timestamp"
  }

  users ||--o{ participants : has
  users ||--o{ messages : has
  users ||--|{ secrets : has
  users ||--|{ refresh_tokens : has
  users ||-- o{ conversations: has
  conversations ||--o{ participants : contains
  conversations ||--o{ messages : has
```

### Flows

#### Registration

```mermaid
flowchart TD
   %% Request registration flow
   subgraph request-registration
       subgraph client
          a0([Start]) --> a1[Submit form with email]
        end

       a1 --> a3
       subgraph server
        a3[Validate email address] --> a4{Email valid?}
        a4 --> |Yes| a5[Search for user with email in DB]
        a5 --> a6{User exists?}
        a6 --> |YES| a7[Return error to client]
        a6 --> |NO| a8[Create a jwt token]
        --> a9[Create registration link /registration?token=...]
        --> a10[Send email to user with registration link]
        --> a11[Response to client]
        --> a12([End    ])
       end
       a7 --> a1
   end

   subgraph registration
        subgraph client-registration
            a20([Start]) --> a21[User click registration link in email]
        end

        subgraph server-registration
            a22[Validate payload]
            --> a23{Payload valid?}
            a23 --> |No| a24[Return error to client]
            a23 --> |yes| a25{Token valid?}
            a25 --> |No| a24
            a25 --> |Yes| a26[Extract email from token]
            --> a27[Create a new user with email, password, firstName, lastname, ...]
            --> a28[Mark user as verified]
            --> a29[Create secrets for user]
            --> a30[Create access token, refresh token for user]
            --> a31[Save user, secrets, refresh token in DB]
            --> a32[Return access token to client]
            --> a33([End])
        end

        a24 --> a20
   end

```

**Forgot password**

```mermaid
flowchart TD
    a0([Client send data]) ---> a1[Server validate data]
    --> a2{Data valid?}
    a2 --> |No| a3[response error to client]
    a2 --> |Yes| a4[find user with correct email]
    a4 --> a5{User exists?}
    a5 --> |No| a6[Response fake to client]
    a5 --> |Yes| a7[Send email to client]
    a6 --> a8([end])
    a7 --> a8
    a3 --> a8

```

### Recover password

```mermaid
flowchart TD
  a0([Client send data]) -->|Form submission| a1[Server validate data]
  --> a2{Token valid?}
  --> |No| a3[Send error to client]
  a2 --> |Yes| a4{Account exists in DB?}
  --> |No| a3
  a2 --> |Yes| a5[Reset user with new password]

```

### Reference

- [Docker download](https://www.docker.com/products/docker-desktop/)
- [Design a chat system](https://bytebytego.com/courses/system-design-objectIderview/design-a-chat-system)
