import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import Joi from 'joi';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerModule } from './logger/logger.module';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { ConversationModule } from './conversation/conversation.module';
import { MessageModule } from './message/message.module';
import { NotificationModule } from './notification/notification.module';
import { CacheModule } from './cache/cache.module';
import { JwtModule } from './jwt/jwt.module';

@Module({
  imports: [
    LoggerModule,
    DatabaseModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        NODE_ENV: Joi.string().allow('development', 'test').required(),
        SERVER_URL: Joi.string().uri().required(),
        DB_CONNECTION_STRING: Joi.string().trim().required(),
        CACHE_CONNECTION_STRING: Joi.string().trim().required(),
        VERIFICATION_MAIL_TOKEN_SECRET: Joi.string().trim().required(),
        VERIFICATION_MAIL_TOKEN_EXPIRE_TIME: Joi.string().required(),
        ACCESS_TOKEN_EXPIRE_TIME: Joi.string().required(),
        REFRESH_TOKEN_EXPIRE_TIME: Joi.string().required(),
        AZURE_COMMUNICATION_SERVICE_CONNECTION_STRING: Joi.string().trim().required(),
        SENDER_EMAIL: Joi.string().trim().required(),
      }),
      isGlobal: true,
    }),
    UserModule,
    ConversationModule,
    MessageModule,
    NotificationModule,
    CacheModule,
    JwtModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
