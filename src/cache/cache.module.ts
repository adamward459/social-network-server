import { Module, CacheModule as NestjsCacheModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { redisStore } from 'cache-manager-redis-store';
import type { RedisClientOptions } from 'redis';

@Module({
  imports: [
    NestjsCacheModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      isGlobal: true,
      useFactory: (configService: ConfigService): RedisClientOptions & { store: any } => {
        return {
          store: redisStore,
          url: configService.get('CACHE_CONNECTION_STRING'),
        };
      },
    }),
  ],
})
export class CacheModule {}
