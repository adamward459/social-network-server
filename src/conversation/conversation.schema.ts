import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { ObjectId } from 'mongoose';
import { Participant } from '../user/participant.schema';
import { User } from '../user/user.schema';

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class Conversation {
  _id: ObjectId;

  @Prop()
  name: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  creator: User;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, name: Participant.name }] })
  participants: Participant[];

  @Prop({ name: 'deleted_at', required: false })
  deletedAt?: Date;

  @Prop({ name: 'updated_at' })
  updatedAt: Date;

  @Prop({ name: 'created_at' })
  createdAt: Date;
}

export const conversationSchema = SchemaFactory.createForClass(Conversation);
