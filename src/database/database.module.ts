import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { DatabaseService } from './database.service';

if (process.env.NODE_ENV !== 'test') {
  mongoose.set('debug', true);
}
@Module({
  imports: [
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      imports: [ConfigModule],
      useFactory: (configService: ConfigService): MongooseModuleOptions => {
        return {
          uri: configService.get('DB_CONNECTION_STRING'),
        };
      },
    }),
  ],
  providers: [DatabaseService],
})
export class DatabaseModule {}
