import { ApiProperty } from '@nestjs/swagger';

export class ErrorDto {
  @ApiProperty()
  statusCode: number;

  @ApiProperty({
    type: Array<string>,
  })
  message: string[];

  @ApiProperty()
  error: string;
}
