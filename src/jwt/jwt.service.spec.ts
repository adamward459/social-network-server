import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule as NestJwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import jwt from 'jsonwebtoken';
import { BadRequestException } from '@nestjs/common';
import { JwtService } from './jwt.service';

describe('JwtService', () => {
  let service: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ isGlobal: true }), NestJwtModule.register({})],
      providers: [JwtService],
    }).compile();

    service = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('verifyForgotPasswordToken', () => {
    it('should throw error when token is invalid', () => {
      expect(() =>
        service.verifyForgotPasswordToken(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjE1MTYyMzkwMjJ9.PBVnIlSU0BSVafRkuyDu8SV_QxRaKOUFMYlhKmk3KTU',
          '1',
        ),
      ).toThrowError(new BadRequestException('Invalid token'));
    });

    it('should return email when token is valid', () => {
      expect(
        service.verifyForgotPasswordToken(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjI1MTYyMzkwMjJ9.7RMJVgHnxiFugZfVj9InasCcGlKJHjD6lJ8wA8D_vFk',
          'secret',
        ),
      ).toEqual('email@email.com');
    });
  });

  describe('decodeForgotPasswordToken', () => {
    it('should work', () => {
      const email = service.decodeForgotPasswordToken(
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjE1MTYyMzkwMjJ9.PBVnIlSU0BSVafRkuyDu8SV_QxRaKOUFMYlhKmk3KTU',
      );
      expect(email).toEqual('email@email.com');
    });
  });

  describe('createForgotPasswordToken', () => {
    it('should work', async function () {
      const email = 'email@email.com';
      const secret = 'secret';
      const token = service.createForgotPasswordToken(email, secret);
      expect(jwt.verify(token, secret)).toBeDefined();
      expect((jwt.decode(token) as { email: string }).email).toEqual(email);
    });
  });

  describe('createVerifyEmailToken', () => {
    it('should work', async function () {
      const email = 'email@email.com';
      const token = service.createVerifyEmailToken(email);
      expect(jwt.verify(token, process.env.VERIFICATION_MAIL_TOKEN_SECRET)).toBeDefined();
      expect((jwt.decode(token) as { email: string }).email).toEqual(email);
    });
  });

  describe('decodeVerifyEmailToken', () => {
    it('should work with valid token', async function () {
      const email = 'email@email.com';
      const token = service.createVerifyEmailToken(email);
      const decodedEmail = service.decodeVerifyEmailToken(token);
      expect(decodedEmail).toEqual(email);
    });

    it('should throw when token is invalid', function () {
      expect(() => service.decodeVerifyEmailToken('123')).toThrowError(new BadRequestException('Invalid token'));
    });
  });

  describe('createAccessToken', () => {
    it('should work', async function () {
      const payload = {
        email: 'email@email.com',
      };
      const secret = 'secret';
      const token = service.createAccessToken(payload, secret);
      expect((jwt.decode(token) as { email: string }).email).toEqual(payload.email);
    });
  });

  describe('createRefreshToken', () => {
    it('should work', async function () {
      const secret = 'secret';
      const token = service.createRefreshToken(secret);
      const decodedToken = jwt.decode(token);
      expect(decodedToken).toBeDefined();
    });
  });
});
