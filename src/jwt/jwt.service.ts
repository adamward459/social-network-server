import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService as NestJwtService } from '@nestjs/jwt';
import { LoggerService } from 'src/logger/logger.service';

@Injectable()
export class JwtService {
  logger = new LoggerService();

  constructor(private readonly nestJwtService: NestJwtService, private readonly configService: ConfigService) {}

  verifyForgotPasswordToken(token: string, secret: string) {
    try {
      return this.nestJwtService.verify<{ email: string }>(token, { secret }).email;
    } catch (error) {
      this.logger.error('[JwtService] [verifyForgotPasswordToken] error', error);
      throw new BadRequestException('Invalid token');
    }
  }

  decodeForgotPasswordToken(token: string) {
    return (this.nestJwtService.decode(token) as { email: string })?.email;
  }

  createForgotPasswordToken(email: string, secret: string) {
    return this.nestJwtService.sign({ email }, { secret, expiresIn: this.configService.get('FORGOT_PASSWORD_TOKEN_EXPIRE_TIME') });
  }

  createVerifyEmailToken(email: string) {
    return this.nestJwtService.sign(
      { email },
      { secret: this.configService.get('VERIFICATION_MAIL_TOKEN_SECRET'), expiresIn: this.configService.get('VERIFICATION_MAIL_TOKEN_EXPIRE_TIME') },
    );
  }

  decodeVerifyEmailToken(token: string) {
    try {
      return this.nestJwtService.verify<{ email: string }>(token, {
        secret: this.configService.get('VERIFICATION_MAIL_TOKEN_SECRET'),
      }).email;
    } catch (error) {
      this.logger.error('[JwtService] [decodeVerifyEmail] error', error);
      throw new BadRequestException('Invalid token');
    }
  }

  createAccessToken(payload: Record<string, unknown> | string, secret: string) {
    return this.nestJwtService.sign(payload, { secret, expiresIn: this.configService.get('ACCESS_TOKEN_EXPIRE_TIME') });
  }

  createRefreshToken(secret: string) {
    return this.nestJwtService.sign({}, { secret, expiresIn: this.configService.get('REFRESH_TOKEN_EXPIRE_TIME') });
  }
}
