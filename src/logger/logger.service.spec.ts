import { Test, TestingModule } from '@nestjs/testing';
import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoggerService],
    }).compile();

    service = module.get<LoggerService>(LoggerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('log should work', () => {
    expect(service.log('message')).toEqual(undefined);
  });

  it('error should work', () => {
    expect(service.error('message')).toEqual(undefined);
  });

  it('warn should work', () => {
    expect(service.warn('message')).toEqual(undefined);
  });

  it('debug should work', () => {
    expect(service.debug('message')).toEqual(undefined);
  });

  it('verbose should work', () => {
    expect(service.verbose('message')).toEqual(undefined);
  });
});
