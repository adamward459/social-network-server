import { Injectable, LoggerService as NestLoggerService } from '@nestjs/common';
import * as winston from 'winston';

@Injectable()
export class LoggerService implements NestLoggerService {
  private _logger: winston.Logger;
  constructor() {
    this._logger = winston.createLogger({
      format: winston.format.combine(winston.format.timestamp(), winston.format.json()),
      transports: [new winston.transports.File({ filename: 'logs/app.log' })],
    });

    if (process.env.NODE_ENV === 'development') {
      this._logger.add(
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json(),
            winston.format.prettyPrint(),
            winston.format.colorize({ all: true }),
          ),
        }),
      );
    }
  }

  log(message: any, ...optionalParams: any[]) {
    this._logger.log('info', message, optionalParams);
  }

  error(message: any, ...optionalParams: any[]) {
    this._logger.error(message, optionalParams);
  }

  warn(message: any, ...optionalParams: any[]) {
    this._logger.warn(message, optionalParams);
  }

  debug?(message: any, ...optionalParams: any[]) {
    this._logger.debug(message, optionalParams);
  }

  verbose?(message: any, ...optionalParams: any[]) {
    this._logger.verbose(message, optionalParams);
  }

  setLogLevels?() {
    throw new Error('Method not implemented.');
  }
}
