import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ErrorDto } from './dto/error.dto';
import { LoggerService } from './logger/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: new LoggerService(),
  });
  app.useLogger(app.get(LoggerService));
  app.enableVersioning();
  app.useGlobalPipes(new ValidationPipe());

  const config = new DocumentBuilder().setTitle('Social network').setVersion('1.0').build();
  const document = SwaggerModule.createDocument(app, config, {
    extraModels: [ErrorDto],
  });
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
