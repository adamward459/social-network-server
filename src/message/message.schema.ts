import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { ObjectId } from 'mongoose';
import { User } from 'src/user/user.schema';

console.log(User);

export enum MessageType {
  // eslint-disable-next-line no-unused-vars
  text = 'text',
}

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class Message {
  _id: ObjectId;

  @Prop()
  message: string;

  @Prop({ enum: MessageType })
  type: MessageType;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  sender: User;

  @Prop({ name: 'deleted_at', required: false })
  deletedAt?: Date;

  @Prop({ name: 'created_at' })
  createdAt: Date;
}

export const messageSchema = SchemaFactory.createForClass(Message);
