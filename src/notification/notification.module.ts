import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { NotificationService } from './notification.service';

@Module({
  imports: [JwtModule.register({})],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationModule {}
