import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from 'src/jwt/jwt.module';
import { LoggerModule } from 'src/logger/logger.module';
import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [JwtModule, ConfigModule.forRoot({ isGlobal: true }), LoggerModule],
      providers: [NotificationService],
    }).compile();

    service = module.get<NotificationService>(NotificationService);
  });

  it('sendVerificationEmail should work', async () => {
    await expect(service.sendVerificationEmail('')).resolves.toBe(undefined);
  });

  it('sendForgotPasswordEmail should work', async () => {
    await expect(service.sendForgotPasswordEmail('', 'secret')).resolves.toBe(undefined);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
