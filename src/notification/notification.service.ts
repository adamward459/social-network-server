import path from 'path';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EmailClient } from '@azure/communication-email';
import { renderFile as ejsRenderFile } from 'ejs';
import { LoggerService } from 'src/logger/logger.service';
import { JwtService } from 'src/jwt/jwt.service';

@Injectable()
export class NotificationService {
  emailClient: EmailClient;

  constructor(private readonly jwtService: JwtService, private readonly configService: ConfigService, private readonly loggerService: LoggerService) {
    this.emailClient = new EmailClient(this.configService.get('AZURE_COMMUNICATION_SERVICE_CONNECTION_STRING'));
  }

  async sendForgotPasswordEmail(email: string, secret: string) {
    const token = this.jwtService.createForgotPasswordToken(email, secret);
    const link = `${this.configService.get('SERVER_URL')}/recover-password?token=${token}`;
    const html = await ejsRenderFile(path.resolve(__dirname, './template/forgot-password.ejs'), { link });
    try {
      if (process.env.NODE_ENV !== 'test') {
        await this.emailClient.send({
          sender: this.configService.get('SENDER_EMAIL'),
          content: {
            subject: 'Forgot your password',
            html,
          },
          recipients: {
            to: [
              {
                email,
              },
            ],
          },
        });
      }
    } catch (error) {
      this.loggerService.error(`Cannot send forgot password email: ${error}, email: ${email}`);
    }
  }

  async sendVerificationEmail(email: string) {
    const token = this.jwtService.createVerifyEmailToken(email);
    const link = `${this.configService.get('SERVER_URL')}/register?token=${token}`;
    const html = await ejsRenderFile(path.resolve(__dirname, './template/verification-mail.ejs'), { link });
    try {
      if (process.env.NODE_ENV !== 'test') {
        await this.emailClient.send({
          sender: this.configService.get('SENDER_EMAIL'),
          content: {
            subject: 'Register your account',
            html,
          },
          recipients: {
            to: [
              {
                email,
              },
            ],
          },
        });
      }
    } catch (error) {
      this.loggerService.error(`Cannot send verification email: ${error}, email: ${email}`);
    }
  }
}
