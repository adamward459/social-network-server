declare global {
  namespace NodeJS {
    export interface ProcessEnv {
      NODE_ENV: 'development' | 'production' | 'test';
      SERVER_URL: string;

      DB_CONNECTION_STRING: string;
      CACHE_CONNECTION_STRING: string;

      VERIFICATION_MAIL_TOKEN_SECRET: string;
      VERIFICATION_MAIL_TOKEN_EXPIRE_TIME: string;

      ACCESS_TOKEN_EXPIRE_TIME: string;
      REFRESH_TOKEN_EXPIRE_TIME: string;

      AZURE_COMMUNICATION_SERVICE_CONNECTION_STRING: string;
      SENDER_EMAIL: string;
    }
  }
}
