import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, Length } from 'class-validator';

export class LoginDto {
  @IsEmail()
  @ApiProperty({
    format: 'email',
  })
  email: string;

  @Length(1, 20)
  @ApiProperty()
  password: string;
}
