import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class RecoverPasswordDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  token: string;

  @Length(1, 20)
  @ApiProperty({
    minLength: 1,
    maxLength: 20,
  })
  password: string;
}
