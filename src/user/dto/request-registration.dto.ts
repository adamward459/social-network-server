import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class RequestRegistrationDto {
  @IsEmail()
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    format: 'email',
  })
  email: string;
}
