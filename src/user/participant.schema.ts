import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Conversation } from '../conversation/conversation.schema';
import { User } from './user.schema';

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class Participant {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name })
  user: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: Conversation.name })
  conversation: Conversation;

  @Prop({ name: 'updated_at', required: false })
  deletedAt?: Date;

  @Prop({ name: 'created_at' })
  createdAt: Date;
}

export const participantSchema = SchemaFactory.createForClass(Participant);
