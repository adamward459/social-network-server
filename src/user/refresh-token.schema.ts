import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class RefreshToken {
  @Prop()
  value: string;

  @Prop()
  device: string;

  @Prop({ name: 'created_at' })
  createdAt: Date;

  @Prop({ name: 'deleted_at', required: false })
  deletedAt?: Date;
}

export const refreshTokenSchema = SchemaFactory.createForClass(RefreshToken);
