import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ObjectId } from 'mongoose';

export enum SecretType {
  // eslint-disable-next-line no-unused-vars
  refreshToken = 'refreshToken',
  // eslint-disable-next-line no-unused-vars
  accessToken = 'accessToken',

  // eslint-disable-next-line no-unused-vars
  forgotPasswordToken = 'forgotPasswordToken',
}

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class Secret {
  _id: ObjectId;

  @Prop({ enum: SecretType })
  type: SecretType;

  @Prop()
  value: string;

  @Prop({ name: 'updated_at' })
  updatedAt: Date;

  @Prop({ name: 'created_at' })
  createdAt: Date;
}

export const secretSchema = SchemaFactory.createForClass(Secret);
