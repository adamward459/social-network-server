import { BadRequestException, CacheModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { JwtModule } from 'src/jwt/jwt.module';
import { JwtService } from 'src/jwt/jwt.service';
import { LoggerModule } from 'src/logger/logger.module';
import { NotificationModule } from 'src/notification/notification.module';
import { RefreshToken } from './refresh-token.schema';
import { Secret, SecretType } from './secret.schema';
import { UserController } from './user.controller';
import { User } from './user.schema';

describe('UserController', () => {
  let controller: UserController;
  let createAccessToken: jest.Mock;
  let decodeVerifyEmailToken: jest.Mock;
  let createRefreshToken: jest.Mock;
  let decodeForgotPasswordToken: jest.Mock;
  let verifyForgotPasswordToken: jest.Mock;
  const request: any = {
    get() {
      return 'device';
    },
  };

  function UserModel() {
    this.save = function () {
      return Promise.resolve();
    };
  }
  UserModel.findOne = jest.fn();

  beforeEach(async () => {
    UserModel.findOne = jest.fn();
    createAccessToken = jest.fn();
    decodeVerifyEmailToken = jest.fn();
    createRefreshToken = jest.fn();
    decodeForgotPasswordToken = jest.fn();
    verifyForgotPasswordToken = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      imports: [NotificationModule, JwtModule, ConfigModule.forRoot({ isGlobal: true }), LoggerModule, CacheModule.register({ isGlobal: true })],
      controllers: [UserController],
      providers: [
        {
          provide: getModelToken(User.name),
          useValue: UserModel,
        },
        {
          provide: JwtService,
          useValue: {
            createAccessToken,
            decodeVerifyEmailToken,
            createRefreshToken,
            decodeForgotPasswordToken,
            verifyForgotPasswordToken,
          },
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('recoverPassword', () => {
    describe('when body.token contains email which does not exist in DB', () => {
      beforeEach(() => {
        decodeForgotPasswordToken.mockReturnValue('');
        UserModel.findOne.mockReturnValue({
          select() {
            return Promise.resolve(undefined);
          },
        });
      });
      it('should throw error', async () => {
        await expect(controller.recoverPassword({ token: '', password: '' })).rejects.toThrowError(new BadRequestException('Invalid token'));
      });
    });

    describe('when email exists but token is invalid', () => {
      beforeEach(() => {
        decodeForgotPasswordToken.mockReturnValue('');
        UserModel.findOne.mockReturnValue({
          select() {
            const user = new User();

            const forgotPasswordSecret = new Secret();
            forgotPasswordSecret.type = SecretType.forgotPasswordToken;
            forgotPasswordSecret.value = 'secret';
            user.secrets = [forgotPasswordSecret];
            return Promise.resolve(user);
          },
        });
        verifyForgotPasswordToken.mockImplementation(() => {
          throw new BadRequestException('Invalid token');
        });
      });
      it('should throw error', async () => {
        await expect(controller.recoverPassword({ token: '', password: '' })).rejects.toThrowError(new BadRequestException('Invalid token'));
      });
    });

    describe('when payload is valid', () => {
      beforeEach(() => {
        decodeForgotPasswordToken.mockReturnValue('');
        UserModel.findOne.mockReturnValue({
          select() {
            const user = new User();

            const forgotPasswordSecret = new Secret();
            forgotPasswordSecret.type = SecretType.forgotPasswordToken;
            forgotPasswordSecret.value = 'secret';
            user.secrets = [forgotPasswordSecret];
            return Promise.resolve({
              ...user,
              async save() {
                return;
              },
            });
          },
        });
        verifyForgotPasswordToken.mockImplementation(() => {
          return true;
        });
      });
      it('should return success message', async () => {
        await expect(controller.recoverPassword({ token: '', password: '' })).resolves.toEqual({
          message: 'Recover password successfully',
        });
      });
    });
  });

  describe('requestRegister', () => {
    describe('when body.email exists in DB', () => {
      beforeEach(() => {
        UserModel.findOne.mockResolvedValue(true);
      });
      it('should return error', async () => {
        await expect(controller.requestRegister({ email: '' })).rejects.toThrow(new BadRequestException('Email is not available'));
      });
    });

    describe('When body.email does not exist in DB', () => {
      beforeEach(() => {
        UserModel.findOne.mockResolvedValue(false);
      });
      it('should return success message', async () => {
        await expect(controller.requestRegister({ email: '' })).resolves.toEqual({
          message: 'Please check your email inbox, we have sent an instruction to verify your email',
        });
      });
    });
  });

  describe('Login', () => {
    describe('when email does not exist in db', () => {
      const email = 'email@email.com';
      beforeEach(() => {
        UserModel.findOne.mockReturnValue({
          select() {
            return Promise.resolve();
          },
        });
      });

      it('should throw error', async function () {
        await expect(controller.logIn({ email, password: '' }, request)).rejects.toStrictEqual(new BadRequestException('Invalid email or password'));
      });
    });

    describe('when password is not matched', () => {
      beforeEach(() => {
        UserModel.findOne.mockReturnValue({
          select() {
            return Promise.resolve(1);
          },
        });
        jest.spyOn(bcrypt, 'compare').mockImplementation(() => Promise.resolve(false));
      });
      it('should throw error', async () => {
        await expect(controller.logIn({ email: '', password: '' }, request)).rejects.toStrictEqual(new BadRequestException('Invalid email or password'));
      });
    });

    describe('when password and email are matched', () => {
      const email = 'email@email.com';
      const password = 'password';

      beforeEach(() => {
        UserModel.findOne.mockReturnValue({
          select() {
            const user = new User();
            user.email = email;
            user.password = '';

            const accessTokenSecret = new Secret();
            accessTokenSecret.type = SecretType.accessToken;
            accessTokenSecret.value = 'secret';

            const refreshTokenSecret = new Secret();
            refreshTokenSecret.type = SecretType.refreshToken;
            refreshTokenSecret.value = 'secret';

            user.secrets = [accessTokenSecret, refreshTokenSecret];

            const refreshToken = new RefreshToken();
            refreshToken.device = 'device';
            refreshToken.value = 'value';
            user.refreshTokens = [refreshToken];
            return Promise.resolve({
              ...user,
              save() {
                return Promise.resolve(true);
              },
            });
          },
        });
        createAccessToken.mockReturnValue(
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjE1MTYyMzkwMjJ9.T3fmU7ocT3CaiLf2p52xFxVIfv-9QPoeA88Vm_ev7js',
        ); // email: email@email.com
        jest.spyOn(bcrypt, 'compare').mockImplementation(() => Promise.resolve(true));
      });

      it('should return accessToken', async function () {
        const { accessToken } = await controller.logIn({ email, password }, request);
        expect(accessToken).toBeDefined();
        expect((jwt.decode(accessToken) as { email: string }).email).toEqual(email);
      });
    });
  });

  describe('forgotPassword', () => {
    beforeEach(() => {
      UserModel.findOne.mockReturnValue({
        select() {
          const user = new User();
          user.email = 'email@email.com';

          const forgotPasswordSecret = new Secret();
          forgotPasswordSecret.value = 'secret';
          forgotPasswordSecret.type = SecretType.forgotPasswordToken;

          user.secrets = [forgotPasswordSecret];
          return Promise.resolve(user);
        },
      });
    });

    it('should work', async function () {
      const result = await controller.forgotPassword({ email: 'email@email.com' });
      expect(result.message).toEqual('Please open your email address');
    });
  });

  describe('register', () => {
    describe('when body.token is invalid', () => {
      beforeEach(() => {
        decodeVerifyEmailToken.mockImplementation(() => {
          throw new BadRequestException('Invalid token');
        });
      });
      it('should return error', async () => {
        await expect(
          controller.register(
            {
              firstName: '',
              lastName: '',
              password: '',
              token: '',
              middleName: '',
            },
            request,
          ),
        ).rejects.toThrow(new BadRequestException('Invalid token'));
      });
    });

    describe('when body.email exists in DB', () => {
      beforeEach(() => {
        UserModel.findOne.mockResolvedValue(true);
      });
      it('should return error', async () => {
        await expect(
          controller.register(
            {
              firstName: '',
              lastName: '',
              password: '',
              token: '',
              middleName: '',
            },
            request,
          ),
        ).rejects.toThrow(new BadRequestException('Invalid token'));
      });
    });

    describe('when payload is valid', () => {
      beforeEach(() => {
        decodeVerifyEmailToken.mockImplementation(() => {
          return;
        });
        UserModel.findOne.mockResolvedValue(null);
        createRefreshToken.mockReturnValue(null);
        createAccessToken.mockReturnValue('token');
      });
      it('should return accessToken', async () => {
        await expect(
          controller.register(
            {
              firstName: '',
              lastName: '',
              password: '',
              token: '',
              middleName: '',
            },
            request,
          ),
        ).resolves.toEqual({
          accessToken: 'token',
        });
      });
    });
  });
});
