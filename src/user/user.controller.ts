import { BadRequestException, Body, CACHE_MANAGER, Controller, HttpCode, HttpStatus, Inject, Post, Req, Version } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags, getSchemaPath } from '@nestjs/swagger';
import bcrypt from 'bcrypt';
import { Cache } from 'cache-manager';
import { Request } from 'express';
import { Model } from 'mongoose';
import { ErrorDto } from 'src/dto/error.dto';
import { JwtService } from 'src/jwt/jwt.service';
import { LoggerService } from 'src/logger/logger.service';
import { NotificationService } from 'src/notification/notification.service';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { LoginDto } from './dto/login.dto';
import { RecoverPasswordDto } from './dto/recover-password.dto';
import { RegisterDto } from './dto/register.dto';
import { RequestRegistrationDto } from './dto/request-registration.dto';
import { RefreshToken } from './refresh-token.schema';
import { Secret, SecretType } from './secret.schema';
import { User, UserDocument } from './user.schema';

const requestRegistrationTimeExp = 60; // 1 min

@Controller('users')
@ApiTags('users')
export class UserController {
  logger = new LoggerService();

  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<UserDocument>,
    private readonly notificationService: NotificationService,
    @Inject(CACHE_MANAGER)
    private readonly cacheManager: Cache,
    private readonly jwtService: JwtService,
  ) {}

  @Version('1')
  @Post('/recover-password')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    description: 'Get `token` from API `forgot-password`',
  })
  @ApiOkResponse({
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            message: {
              type: 'string',
            },
          },
        },
        example: {
          message: 'Recover password successfully',
        },
      },
    },
  })
  async recoverPassword(@Body() body: RecoverPasswordDto) {
    const email = this.jwtService.decodeForgotPasswordToken(body.token);
    const existingUserWithEmail = await this.userModel
      .findOne({
        email,
      })
      .select('email secrets');
    if (!existingUserWithEmail) {
      throw new BadRequestException('Invalid token');
    }

    const secretIndex = existingUserWithEmail.secrets.findIndex((s) => s.type === SecretType.forgotPasswordToken);
    this.jwtService.verifyForgotPasswordToken(body.token, existingUserWithEmail.secrets[secretIndex].value);

    existingUserWithEmail.secrets[secretIndex].value = existingUserWithEmail.email + SecretType.forgotPasswordToken + Date.now();
    existingUserWithEmail.password = await bcrypt.hash(body.password, 10);
    await existingUserWithEmail.save();

    return {
      message: 'Recover password successfully',
    };
  }

  @Version('1')
  @Post('/forgot-password')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    description: 'Email will expire after 1day',
  })
  @ApiOkResponse({
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            message: {
              type: 'string',
            },
          },
        },
        example: {
          message: 'Please open your email address',
        },
      },
    },
  })
  async forgotPassword(@Body() body: ForgotPasswordDto) {
    const email = body.email.toLowerCase();
    /**
     * For security reason we do not inform users that email does not exist
     */
    const successResponse = {
      message: 'Please open your email address',
    };
    const existingUserWithEmail = await this.userModel
      .findOne({
        email,
      })
      .select('email secrets');
    if (existingUserWithEmail) {
      this.notificationService.sendForgotPasswordEmail(
        existingUserWithEmail.email,
        existingUserWithEmail.secrets.find((s) => s.type === SecretType.forgotPasswordToken).value,
      );
    }
    return successResponse;
  }

  @Version('1')
  @Post('/login')
  @HttpCode(HttpStatus.OK)
  async logIn(@Body() body: LoginDto, @Req() req: Request) {
    const email = body.email.toLowerCase();
    const failResponse = new BadRequestException('Invalid email or password');

    const existingUserWithEmail = await this.userModel
      .findOne({
        email,
      })
      .select('email password secrets refreshTokens');
    if (!existingUserWithEmail) {
      throw failResponse;
    }

    const isPasswordMatch = await bcrypt.compare(body.password, existingUserWithEmail.password);
    if (!isPasswordMatch) {
      throw failResponse;
    }

    const device = req.get('user-agent');
    const foundIndex = existingUserWithEmail.refreshTokens.findIndex((r) => r.device === device);
    const refreshTokenSecret = existingUserWithEmail.secrets.find((s) => s.type === SecretType.refreshToken).value;
    if (foundIndex !== -1) {
      existingUserWithEmail.refreshTokens[foundIndex].value = this.jwtService.createRefreshToken(refreshTokenSecret);
    } else {
      const refreshToken = new RefreshToken();
      refreshToken.device = device;
      refreshToken.value = this.jwtService.createRefreshToken(refreshTokenSecret);
      existingUserWithEmail.refreshTokens.push(refreshToken);
    }
    await existingUserWithEmail.save();

    return {
      accessToken: this.jwtService.createAccessToken({ email: body.email }, existingUserWithEmail.secrets.find((s) => s.type === SecretType.accessToken).value),
    };
  }

  @Version('1')
  @Post('/register')
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({
    description: 'Get `token` from API `request-registration`',
  })
  @ApiCreatedResponse({
    schema: {
      type: 'object',
      properties: {
        accessToken: {
          type: 'string',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    content: {
      'application/json': {
        schema: {
          $ref: getSchemaPath(ErrorDto),
        },
        examples: {
          'invalid-token': {
            value: {
              statusCode: HttpStatus.BAD_REQUEST,
              message: ['Invalid token'],
              error: 'Bad request',
            },
          },
        },
      },
    },
  })
  async register(@Body() body: RegisterDto, @Req() req: Request) {
    const email = this.jwtService.decodeVerifyEmailToken(body.token);
    const existingUserWithEmail = await this.userModel.findOne({
      email,
    });
    if (existingUserWithEmail) {
      throw new BadRequestException('Invalid token');
    }

    const user = new this.userModel();
    user.email = email;
    user.password = await bcrypt.hash(body.password, 10);
    user.firstName = body.firstName;
    user.middleName = body.middleName;
    user.lastName = body.lastName;
    user.lastActiveAt = new Date();
    user.isVerified = true;

    const accessTokenSecret = new Secret();
    accessTokenSecret.type = SecretType.accessToken;
    accessTokenSecret.value = user.email + SecretType.accessToken + Date.now();

    const refreshTokenSecret = new Secret();
    refreshTokenSecret.type = SecretType.refreshToken;
    refreshTokenSecret.value = user.email + SecretType.refreshToken + Date.now();

    const forgotPasswordTokenSecret = new Secret();
    forgotPasswordTokenSecret.type = SecretType.forgotPasswordToken;
    forgotPasswordTokenSecret.value = user.email + SecretType.forgotPasswordToken + Date.now();

    user.secrets = [accessTokenSecret, refreshTokenSecret, forgotPasswordTokenSecret];

    const refreshToken = new RefreshToken();
    refreshToken.device = req.get('user-agent');
    refreshToken.value = this.jwtService.createRefreshToken(refreshTokenSecret.value);

    user.refreshTokens = [refreshToken];

    await user.save();

    return {
      accessToken: this.jwtService.createAccessToken({ email }, accessTokenSecret.value),
    };
  }

  @Version('1')
  @Post('/request-registration')
  @HttpCode(HttpStatus.OK)
  @ApiOperation({
    description: 'This API will send an email to a user with link to create his account. In that link there is a `token` with expiration `1 day`',
  })
  @ApiOkResponse({
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            message: {
              type: 'string',
            },
          },
        },
        example: {
          message: 'Please check your email inbox, we have sent an instruction to verify your email',
        },
      },
    },
  })
  @ApiBadRequestResponse({
    content: {
      'application/json': {
        schema: {
          $ref: getSchemaPath(ErrorDto),
        },
        examples: {
          'email-unavailable': {
            value: {
              statusCode: HttpStatus.BAD_REQUEST,
              message: ['Email is not available'],
              error: 'Bad request',
            },
          },
        },
      },
    },
  })
  async requestRegister(@Body() body: RequestRegistrationDto) {
    const email = body.email.toLowerCase();
    const successResponse = {
      message: 'Please check your email inbox, we have sent an instruction to verify your email',
    };
    const userWithEmailCache = await this.cacheManager.get(email);
    if (userWithEmailCache) {
      return successResponse;
    }
    const userWithEmail = await this.userModel.findOne({
      where: {
        email,
      },
    });
    if (userWithEmail) {
      throw new BadRequestException('Email is not available');
    }

    this.notificationService.sendVerificationEmail(email);
    await this.cacheManager.set(email, true, { ttl: requestRegistrationTimeExp } as any);
    return successResponse;
  }
}
