import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from 'src/jwt/jwt.module';
import { NotificationModule } from 'src/notification/notification.module';
import { UserController } from './user.controller';
import { User, userSchema } from './user.schema';
import { UserService } from './user.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        schema: userSchema,
        name: User.name,
      },
    ]),
    NotificationModule,
    JwtModule,
  ],
  providers: [UserService],
  controllers: [UserController],
})
export class UserModule {}
