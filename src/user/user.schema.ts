import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { ObjectId } from 'mongoose';
import { RefreshToken, refreshTokenSchema } from './refresh-token.schema';
import { Secret, secretSchema } from './secret.schema';

@Schema({
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
})
export class User {
  _id: ObjectId;

  @Prop({ name: 'first_name' })
  firstName: string;

  @Prop({ name: 'middle_name', required: false })
  middleName?: string;

  @Prop({ name: 'last_name' })
  lastName: string;

  @Prop({ unique: true })
  email: string;

  @Prop()
  password: string;

  @Prop({ name: 'is_verified' })
  isVerified: boolean;

  @Prop({ name: 'last_active_at' })
  lastActiveAt: Date;

  @Prop({ type: [secretSchema], length: 2 })
  secrets: Secret[];

  @Prop({ type: [refreshTokenSchema] })
  refreshTokens: RefreshToken[];

  @Prop({ name: 'deleted_at', nullable: true })
  deletedAt?: Date;

  @Prop({ name: 'updated_at' })
  updatedAt: Date;

  @Prop({ name: 'created_at' })
  createdAt: Date;
}

export type UserDocument = User & mongoose.Document;
export const userSchema = SchemaFactory.createForClass(User);
