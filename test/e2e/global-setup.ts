import path from 'path';
import { config } from 'dotenv';
import { MongoMemoryServer } from 'mongodb-memory-server';

config({ path: path.resolve(__dirname, '../../.env.test') });

export default async function globalSetup() {
  const instance = await MongoMemoryServer.create({
    instance: {
      port: 27018,
      dbName: 'social-network',
    },
  });
  const uri = instance.getUri();
  process.env.DB_CONNECTION_STRING = uri.replace('127.0.0.1', 'localhost');
  global.MONGO_INSTANCE = instance;
}
