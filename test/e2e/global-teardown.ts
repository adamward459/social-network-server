export default async function tearDown() {
  await global.MONGO_INSTANCE.stop();
}
