import { CacheModule, HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import request from 'supertest';
import { DatabaseModule } from 'src/database/database.module';
import { LoggerModule } from 'src/logger/logger.module';
import { UserModule } from 'src/user/user.module';
import { SecretType } from 'src/user/secret.schema';

describe('/v1/users/login', () => {
  let app: INestApplication;
  let server;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        UserModule,
        LoggerModule,
        CacheModule.register({ isGlobal: true }),
        DatabaseModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.enableVersioning();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    server = app.getHttpServer();
  });

  afterEach(async () => {
    await app.close();
    await server.close();
  });

  describe('when payload is missing', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({});
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be an email', 'password must be longer than or equal to 1 characters']);
    });
  });

  describe('when payload.email is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 123,
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be an email']);
    });
  });

  describe('when payload.email is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: '',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be an email']);
    });
  });

  describe('when payload.email is not a valid email format', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'abc',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be an email']);
    });
  });

  describe('when payload.password is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'email@email.com',
        password: 123,
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be longer than or equal to 1 and shorter than or equal to 20 characters']);
    });
  });

  describe('when payload.password is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'email@email.com',
        password: '',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be longer than or equal to 1 characters']);
    });
  });

  describe('when payload.password is longer than 20 characters', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server)
        .post('/v1/users/register')
        .send({
          token: 'token',
          firstName: 'firstName',
          lastName: 'lastName',
          password: Array.from({ length: 21 }, (_, index) => index + '').join(''),
        });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be shorter than or equal to 20 characters']);
    });
  });

  describe('when payload.email does not exist in db', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.db.dropDatabase();
      await mongoose.disconnect();
    });
    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'email@email.com',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid email or password');
    });
  });

  describe('when payload.password is a wrong password', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email: 'email@email.com',
        },
        {
          $set: {
            email: 'email@email.com',
            password: 'hashedPassword',
          },
        },
        {
          upsert: true,
        },
      );
      await mongoose.disconnect();
    });

    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'email@email.com',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid email or password');
    });
  });

  describe('when payload.password and payload.email is correct', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email: 'email@email.com',
        },
        {
          $set: {
            email: 'email@email.com',
            password: '$2a$10$f9MfNiYDRtey1GNq26SNLO5uR8S3zNMSsrdReVk9knc1dgm4F1f8y',
            secrets: [
              {
                type: SecretType.accessToken,
                value: 'secret',
              },
              {
                type: SecretType.refreshToken,
                value: 'secret',
              },
            ],
            refreshTokens: [
              {
                device: 'test',
                value: 'secret',
              },
            ],
          },
        },
        { upsert: true },
      );
      await mongoose.disconnect();
    });

    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/login').send({
        email: 'email@email.com',
        password: '123456',
      });
      expect(response.statusCode).toEqual(HttpStatus.OK);
      expect(response.body.accessToken).toBeDefined();
    });
  });
});
