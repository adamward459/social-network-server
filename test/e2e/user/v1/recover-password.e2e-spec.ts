import { CacheModule, HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import request from 'supertest';
import jwt from 'jsonwebtoken';
import { DatabaseModule } from 'src/database/database.module';
import { LoggerModule } from 'src/logger/logger.module';
import { UserModule } from 'src/user/user.module';
import { SecretType } from '../../../../src/user/secret.schema';

describe('/v1/users/recover-password', () => {
  let app: INestApplication;
  let server;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        UserModule,
        LoggerModule,
        CacheModule.register({ isGlobal: true }),
        DatabaseModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.enableVersioning();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    server = app.getHttpServer();
  });

  afterEach(async () => {
    await app.close();
    await server.close();
  });

  describe('when payload is missing', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({});
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['token should not be empty', 'token must be a string', 'password must be longer than or equal to 1 characters']);
    });
  });

  describe('when payload.token is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['token should not be empty', 'token must be a string']);
    });
  });

  describe('when payload.token is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({
        token: '',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['token should not be empty']);
    });
  });

  describe('when payload.password is longer than 20 characters', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({
        token: 'token',
        password: '111111111111111111111',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be shorter than or equal to 20 characters']);
    });
  });

  describe('when payload.password is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({
        token: 'token',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be longer than or equal to 1 characters']);
    });
  });

  describe('when payload.token decodes an email which does not exist in db', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.db.dropDatabase();
      await mongoose.disconnect();
    });
    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/recover-password').send({
        token: 'token',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid token');
    });
  });

  describe('when payload.token is not valid', () => {
    const email = 'email@email.com';
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email,
        },
        {
          $set: {
            email,
            secrets: [
              {
                type: SecretType.forgotPasswordToken,
                value: 'secret',
              },
            ],
          },
        },
        { upsert: true },
      );
      await mongoose.disconnect()
    });
    it('should return status 400 and error messages', async function () {
      const token = jwt.sign(
        {
          email,
        },
        'abc',
        { expiresIn: '100000d' },
      );

      const response = await request(server).post('/v1/users/recover-password').send({
        token,
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid token');
    });
  });

  describe('when payload is valid', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email: 'email@email.com',
        },
        {
          $set: {
            email: 'email@email.com',
            secrets: [
              {
                type: SecretType.forgotPasswordToken,
                value: 'secret',
              },
            ],
          },
        },
        { upsert: true },
      );
      await mongoose.disconnect()
    });
    it('should return status 400 and error messages', async function () {
      const token = jwt.sign({ email: 'email@email.com' }, 'secret', {
        expiresIn: '10000d',
      });

      const response = await request(server).post('/v1/users/recover-password').send({
        token,
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.OK);
      expect(response.body.message).toEqual('Recover password successfully');
    });
  });
});
