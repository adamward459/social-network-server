import { CacheModule, HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import request from 'supertest';
import { DatabaseModule } from 'src/database/database.module';
import { LoggerModule } from 'src/logger/logger.module';
import { UserModule } from 'src/user/user.module';

describe('/v1/users/register', () => {
  let app: INestApplication;
  let server;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        UserModule,
        LoggerModule,
        CacheModule.register({ isGlobal: true }),
        DatabaseModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.enableVersioning();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    server = app.getHttpServer();
  });

  afterEach(async () => {
    await app.close();
    await server.close();
  });

  describe('when payload is missing', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({});
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual([
        'token should not be empty',
        'token must be a string',
        'firstName should not be empty',
        'firstName must be a string',
        'lastName should not be empty',
        'lastName must be a string',
        'password must be longer than or equal to 1 characters',
        'password must be a string',
      ]);
    });
  });

  describe('when payload.token is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 123,
        firstName: 'firstName',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['token must be a string']);
    });
  });

  describe('when payload.token is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: '',
        firstName: 'firstName',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['token should not be empty']);
    });
  });

  describe('when payload.firstName is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: 123,
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['firstName must be a string']);
    });
  });

  describe('when payload.firstName is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: '',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['firstName should not be empty']);
    });
  });

  describe('when payload.lastName is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: 'firstName',
        lastName: 123,
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['lastName must be a string']);
    });
  });

  describe('when payload.lastName is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: 'firstName',
        lastName: '',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['lastName should not be empty']);
    });
  });

  describe('when payload.password is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: 'firstName',
        lastName: 'lastName',
        password: 123,
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual([
        'password must be longer than or equal to 1 and shorter than or equal to 20 characters',
        'password must be a string',
      ]);
    });
  });

  describe('when payload.password is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token: 'token',
        firstName: 'firstName',
        lastName: 'lastName',
        password: '',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be longer than or equal to 1 characters']);
    });
  });

  describe('when payload.password is longer than 20 characters', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server)
        .post('/v1/users/register')
        .send({
          token: 'token',
          firstName: 'firstName',
          lastName: 'lastName',
          password: Array.from({ length: 21 }, (_, index) => index + '').join(''),
        });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['password must be shorter than or equal to 20 characters']);
    });
  });

  describe('whe payload.token is malformed or expired', () => {
    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/register').send({
        token:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJ4QHguY29tIiwiaWF0IjoyNTE2MjM5MDIyfQ.Vfl7LbGlWyJUp9ZhgkiNkGnjgVOTTEbaQx6EMRZDIvM',
        firstName: 'firstName',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid token');
    });
  });

  describe('when payload.token provides an email which is in db', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email: 'email@email.com',
        },
        {
          $set: {
            email: 'email@email.com',
          },
        },
        { upsert: true },
      );
      await mongoose.disconnect();
    });

    it('should return status 400 and error message', async () => {
      /**
       *{
       * "sub": "1234567890",
       * "email": "email@email.com",
       * "iat": 2516239022
       *}
       */
      const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjI1MTYyMzkwMjJ9.7RMJVgHnxiFugZfVj9InasCcGlKJHjD6lJ8wA8D_vFk';
      const response = await request(server).post('/v1/users/register').send({
        token,
        firstName: 'firstName',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Invalid token');
    });
  });

  describe('when payload is valid', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.db.dropDatabase();
      await mongoose.disconnect();
    });
    it('should return status 200 and access token', async () => {
      /**
       *{
       * "sub": "1234567890",
       * "email": "email@email.com",
       * "iat": 2516239022
       *}
       */
      const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJlbWFpbEBlbWFpbC5jb20iLCJpYXQiOjI1MTYyMzkwMjJ9.7RMJVgHnxiFugZfVj9InasCcGlKJHjD6lJ8wA8D_vFk';
      const response = await request(server).post('/v1/users/register').send({
        token,
        firstName: 'firstName',
        lastName: 'lastName',
        password: 'password',
      });
      expect(response.statusCode).toEqual(HttpStatus.CREATED);
      expect(response.body.accessToken).toBeDefined();
    });
  });
});
