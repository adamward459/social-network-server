import { CacheModule, HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import mongoose from 'mongoose';
import request from 'supertest';
import { DatabaseModule } from 'src/database/database.module';
import { LoggerModule } from 'src/logger/logger.module';
import { UserModule } from 'src/user/user.module';

describe('/v1/users/request-registration', () => {
  let app: INestApplication;
  let server;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        UserModule,
        LoggerModule,
        CacheModule.register({ isGlobal: true }),
        DatabaseModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.enableVersioning();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    server = app.getHttpServer();
  });

  afterEach(async () => {
    await app.close();
    await server.close();
  });

  describe('when payload is missing', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({});
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be a string', 'email should not be empty', 'email must be an email']);
    });
  });

  describe('when payload.email is not a string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({ email: 123 });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be a string', 'email must be an email']);
    });
  });

  describe('when payload.email is a empty string', () => {
    it('should return status 400 and error messages', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({ email: '' });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email should not be empty', 'email must be an email']);
    });
  });

  describe('when payload.email is not a valid email format', () => {
    it('should return status 400', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({ email: 'email' });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual(['email must be an email']);
    });
  });

  describe('when there is a user with email existing in DB', () => {
    const email = 'email@email.com';
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.collection('users').updateOne(
        {
          email,
        },
        {
          $set: {
            email,
          },
        },
        { upsert: true },
      );
      await mongoose.disconnect();
    });

    it('should return status 400 and error message', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({ email });
      expect(response.statusCode).toEqual(HttpStatus.BAD_REQUEST);
      expect(response.body.message).toEqual('Email is not available');
    });
  });

  describe('When there is no user with email in DB', () => {
    beforeAll(async () => {
      await mongoose.connect(process.env.DB_CONNECTION_STRING);
      await mongoose.connection.db.dropDatabase();
      await mongoose.disconnect();
    });
    it('should return status 200', async () => {
      const response = await request(server).post('/v1/users/request-registration').send({
        email: 'email@email.com',
      });
      expect(response.statusCode).toEqual(HttpStatus.OK);
      expect(response.body).toEqual({
        message: 'Please check your email inbox, we have sent an instruction to verify your email',
      });
    });
  });
});
